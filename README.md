# MGT6203 -- Group project #
README author: Yutaro Watanabe

It would be nice if we could share the work between us in this unified repo.
Please feel free to edit/suggest changes in files and guidelines 

### How should we manage? ###
- In order to keep the repo organized, we should keep everything in a folder.
(ex. ML codes should not live together with the dataset folder)
- We don't have to use `.gitignore` but feel free to do in case that helps with cleanliness.

### Contribution guidelines ###

Small changes such as continuation of past code, lint fixes, etc. may be directly worked on and pushed to `master`.

Every new "big/experimental" changes and contributions should be done on a separate branch, 
then later confirmed by the rest of the team to merge it via pull/merge requests.

Apart from the aforementioned merge against the `master` branch, let's keep a linear graph.
(i.e. every other branch should not merge, apart from `fast-forward` capable situations.)
Every time a "merge-like" action is required, we should use a rebase to keep a clean/linear graph, 
avoiding a big fat mess in a repo.

Let's also use [conventional commit](https://gist.github.com/qoomon/5dfcdf8eec66a051ecd85625518cfd13) 
commit styling in order to keep the commits organized as well.
