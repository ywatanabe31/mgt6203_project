cinemagoer==2022.2.11
pandas==1.4.1
numpy==1.22.3
jupyter==1.0.0
tqdm==4.63.1