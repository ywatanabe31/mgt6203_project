import spacy
import pandas as pd
import re

nlp = spacy.load("en_core_web_sm")
stopwords = nlp.Defaults.stop_words
letters = re.compile(r'[^a-zA-Z ]+')

def remove_stopwords_and_punctuation(sentence: str) -> str:
    tokens = sentence.split(" ")
    filtered = [token for token in tokens if not token in stopwords]
    string_form = (" ").join(filtered)
    return re.sub(letters, '', string_form)

data = pd.read_csv('/home/ksenator/school_projects/mgt6203_project/data/merged_20000_w_plots_nona.csv')
data["keywords"] = ""
for i, row in data.iterrows():
    doc = nlp(row['plot'])
    keywords = ""
    for sent in doc.sents:
        good_out = remove_stopwords_and_punctuation(sent.text)
        keywords += f"{good_out} "

    data.loc[i, "keywords"] = keywords

data.to_csv('tokenized.csv', index=False)
